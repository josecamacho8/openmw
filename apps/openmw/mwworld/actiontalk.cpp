#include "actiontalk.hpp"

#include "../mwbase/environment.hpp"
#include "../mwbase/windowmanager.hpp"

#include "../mwmechanics/actorutil.hpp"
#include "../mwmechanics/creaturestats.hpp"
#include "../mwmechanics/npcstats.hpp"

#include "../mwworld/player.hpp"
#include "../mwworld/class.hpp"


namespace MWWorld
{
    ActionTalk::ActionTalk (const Ptr& actor) : Action (false, actor) {}

    void ActionTalk::executeImp (const Ptr& actor)
    {
        if (actor == MWMechanics::getPlayer())
        {
            // Assign crimeid to guard after player character
            // voluntarily talks to guard with bounty on head.
            // Must do to give guard a chance to remove crimeid
            // after player clears bounty. See Actors::updateCrimePursuit.

            MWWorld::Ptr targetActor = getTarget();
            if (targetActor.getClass().isClass(targetActor, "Guard")) 
            {
                MWMechanics::CreatureStats& creatureStats = targetActor.getClass().getCreatureStats(targetActor);
                MWMechanics::NpcStats& npcStats = targetActor.getClass().getNpcStats(targetActor);
                int bounty = actor.getClass().getNpcStats(actor).getBounty();

                if(creatureStats.getMagicEffects().get(ESM::MagicEffect::CalmHumanoid).getMagnitude() == 0 && bounty > 0)
                {
                    npcStats.setCrimeId(MWBase::Environment::get().getWorld()->getPlayer().getNewCrimeId());
                }
            }

            MWBase::Environment::get().getWindowManager()->pushGuiMode(MWGui::GM_Dialogue, getTarget());
        }
    }
}
